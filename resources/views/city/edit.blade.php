@extends('layouts.main')

@section('content')
<?php
    
    $province_id = $city->province_id;
    if(old('province_id')) $province_id = old('province_id');
    $province_name = \App\Models\Province::where('id', intval($province_id))->first()->name;
    $val_province = '<option value="'.$province_id.'" selected>'.$province_name.'</option>';

    $city_name = $city->name;
    if(old('name')) $city_name = old('name');

    $is_active = '';
    if($city->is_active || old('is_active')) $is_active = ' checked';
?>
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?= $title ?></h4>
    </div>
</div>
<div class="row">
    <form action="/{{ $dir }}/{{ $city->id }}" method="post">
        @csrf
        @method('PUT')
        <div class="col-md-6">
            <div class="card">
                <div class="card-header p-0 bg-primary text-center">
                    <div class="mt-2 text-white card-title">Formulir</div>
                </div>
                <div class="card-body">
                    <div class="form-group @error('province_id') has-danger @enderror">
                        <label class="form-label required">Nama Provinsi</label>
                        <select name="province_id" id="province_id" class="form-control select2">{!! $val_province !!}</select>
                        @error('province_id')
                        <small class="form-control-feedback">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group @error('name') has-danger @enderror">
                        <label class="form-label required">Nama Kota/Kabupaten</label>
                        <input value="{{ $city_name }}" type="text" name="name" id="name" class="form-control" placeholder="Nama Kota/Kabupaten" autocomplete="off" required>
                        @error('name')
                        <small class="form-control-feedback">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="form-label">Aktif?</label>
                        <div class="form-check form-switch">
                            <input type="checkbox" class="form-check-input input_switch"{{ $is_active }} id="is_active" value="1">
                          <input type="hidden" name="is_active" value="1">
                          <label class="form-check-label" for="is_active">Aktif</label>
                        </div> 
                    </div>
                </div>
                <div class="card-footer">
                    <div class="float-end">
                        <div class="btn-group m-l-15">
                            <a href="/{{ $dir }}" type="button" class="btn waves-effect waves-light btn-outline-primary">Kembali</a>
                            <button type="submit" class="btn btn-primary btn-save">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('js')
<script src="{{ url('assets/node_modules/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript">
    $('.input_switch').trigger('change');
    $('#province_id').select2({
        placeholder: '-- Pilih Provinsi --',
        ajax: {
            url: '/province-autocomplete-search',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });
</script>
@endsection