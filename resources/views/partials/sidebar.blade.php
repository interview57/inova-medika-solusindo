<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User Profile-->
        <div class="user-profile">
            <div class="user-pro-body">
                <div>
                    <img src="{{ url('assets/images/no-image.png') }}" alt="user-img" class="img-circle">
                </div>
                <div class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle u-dropdown link hide-menu" data-bs-toggle="dropdown" role="button" aria-haspopup="true"
                        aria-expanded="false">{{ $user->name }}
                        <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu animated flipInY">
                        <!-- text-->
                        <a href="javascript:void(0)" class="dropdown-item">
                            <i class="ti-user"></i> My Profile</a>
                        <div class="dropdown-divider"></div>
                        <!-- text-->
                        <form action="/logout" method="post">
                            @csrf
                            <button type="submit" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a></button>
                        </form>
                        <!-- text-->
                    </div>
                </div>
            </div>
        </div>
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap" title="MASTER">--- MASTER</li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="javascript:;" aria-expanded="false">
                        <i class="fas fa-list-alt"></i> <span class="hide-menu">Wilayah</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a class="waves-effect waves-dark" href="/province" aria-expanded="false" title="Users">
                                <i class="fas fa-list-alt"></i> <span class="hide-menu">Provinsi</span>
                            </a>
                        </li>
                        <li>
                            <a class="waves-effect waves-dark" href="/city" aria-expanded="false" title="Users">
                                <i class="fas fa-list-alt"></i> <span class="hide-menu">Kota/Kabupaten</span>
                            </a>
                        </li>
                        <li>
                            <a class="waves-effect waves-dark" href="/district" aria-expanded="false" title="Users">
                                <i class="fas fa-list-alt"></i> <span class="hide-menu">Kecamatan</span>
                            </a>
                        </li>
                    </ul>
                </li>       
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="javascript:;" aria-expanded="false">
                        <i class="ti-settings"></i> <span class="hide-menu">User Management</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a class="waves-effect waves-dark" href="/user" aria-expanded="false" title="Users">
                                <i class="fas fa-user-plus"></i> <span class="hide-menu">Users</span>
                            </a>
                        </li>
                        <li>
                            <a class="waves-effect waves-dark" href="/employee" aria-expanded="false" title="Pegawai">
                                <i class="fas fa-user"></i> <span class="hide-menu">Pegawai</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="/action" aria-expanded="false" title="Tindakan">
                        <i class="ti-direction-alt"></i> <span class="hide-menu">Tindakan</span>
                    </a>
                </li> 
                <li>
                    <a class="waves-effect waves-dark" href="/medicine" aria-expanded="false" title="Obat">
                        <i class="fas fa-file-medical"></i> <span class="hide-menu">Obat</span>
                    </a>
                </li>     
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>