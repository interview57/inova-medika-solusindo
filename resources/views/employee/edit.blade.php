@extends('layouts.main')

@section('content')
<?php
    $name = $employee->name;
    if(old('name')) $name = old('name');

    $address = $employee->address;
    if(old('address')) $address = old('address');

    $phone = $employee->phone;
    if(old('phone')) $phone = old('phone');

    $male = '';
    if(old('gender') == 1 || (!old('gender') && $employee->gender == 1)) $male = ' checked';

    $female = '';
    if(old('gender') == 2 || (!old('gender') && $employee->gender == 2)) $female = ' checked';

    $is_active = '';
    if((strlen(old('is_active'))==0 && $employee->is_active) || old('is_active')) $is_active = ' checked';
?>
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?= $title ?></h4>
    </div>
</div>
<div class="row">
    <form action="/{{ $dir }}/{{ $employee->id }}" method="post">
        @csrf
        @method('PUT')
        <div class="col-md-6">
            <div class="card">
                <div class="card-header p-0 bg-primary text-center">
                    <div class="mt-2 text-white card-title">Formulir</div>
                </div>
                <div class="card-body">
                    <div class="form-group @error('name') has-danger @enderror">
                        <label class="form-label required">Nama</label>
                        <input value="{{ $name }}" type="text" name="name" id="name" class="form-control" placeholder="Nama" autocomplete="off" required>
                        @error('name')
                        <small class="form-control-feedback">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group @error('gender') has-danger @enderror">
                        <label for="form-label">Jenis Kelamin</label>
                        <div class="col-sm-12">
                            <div class="custom-control custom-radio">
                                <input type="radio" id="male" name="gender" value="1" class="form-check-input"{{ $male }}>
                                <label class="form-check-label" for="male">Laki-laki</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="female" name="gender" value="2" class="form-check-input"{{ $female }}>
                                <label class="form-check-label" for="female">Perempuan</label>
                            </div>
                        </div>
                        @error('gender')
                        <small class="form-control-feedback">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group @error('address') has-danger @enderror">
                        <label class="form-label">Alamat</label>
                        <textarea class="form-control" name="address" id="address" rows="2">{{ $address }}</textarea>
                        @error('address')
                        <small class="form-control-feedback">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group @error('phone') has-danger @enderror">
                        <label class="form-label required">No Telepon</label>
                        <input value="{{ $phone }}" type="text" name="phone" id="phone" class="form-control" placeholder="No Telepon" autocomplete="off" required>
                        @error('phone')
                        <small class="form-control-feedback">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="form-label">Aktif?</label>
                        <div class="form-check form-switch">
                            <input type="checkbox" class="form-check-input input_switch"{{ $is_active }} id="is_active" value="1">
                          <input type="hidden" name="is_active" value="1">
                          <label class="form-check-label" for="is_active">Aktif</label>
                        </div> 
                    </div>
                </div>
                <div class="card-footer">
                    <div class="float-end">
                        <div class="btn-group m-l-15">
                            <a href="/{{ $dir }}" type="button" class="btn waves-effect waves-light btn-outline-primary">Kembali</a>
                            <button type="submit" class="btn btn-primary btn-save">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $('.input_switch').trigger('change');
</script>
@endsection