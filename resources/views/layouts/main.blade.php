<?php
    $app_name = "Inova Medika Sousindo";
    $user = Auth::user();
?>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="{{ $title.' - Inova Medika Solusindo' }}">
    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="author" content="mw">
    <meta http-equiv="content-language" content="id">
    <!-- Media -->
    <meta property="og:title" content="{{ $title.' - Inova Medika Solusindo' }}"/>
    <meta property="og:site_name" content="{{ $title.' - Inova Medika Solusindo' }}"/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content="{{ url('/') }}"/>
    <meta property="og:description" content=""/>
    <meta property="og:image:secure_url" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="id_ID" />
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="">
    <link rel="shortcut icon" href="">
    <title>{{ $title.' - Inova Medika Solusindo' }}</title>
    <!-- Custom CSS -->
    <link href="{{ url('assets/node_modules/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('assets/css/style.min.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <link href="{{ url('assets/css/backend/custom.css') }}" rel="stylesheet">
    <link href="{{ url('assets/css/backend/custom_style.css') }}" rel="stylesheet">
    @yield('css')
</head>

<body class="skin-default fixed-layout">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Inova Medika Solusindo</p>
        </div>
    </div>
    <!-- Main wrapper - style you can find in pages.scss -->
    <div id="main-wrapper">
        <!-- Topbar header -->
        @include('partials.header')
        <!-- End Topbar header -->

        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        @include('partials.sidebar')
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Page Content -->
                @yield('content')
                <!-- End Page Content -->

                <!-- Right sidebar -->
                @include('partials.rigth_sidebar')
                <!-- End Right sidebar -->
            </div>
            <!-- End Container fluid  -->
        </div>
        <!-- End Page wrapper  -->
        
        <!-- footer -->
        <footer class="footer">
            © 2022 Muhamad Wildan
            <a href="<?= url('/') ?>">Inova Media Solusindo</a>
        </footer>
        <!-- End footer -->
    </div>
    <!-- End Wrapper -->
    
    <!-- All Jquery -->
    <script src="{{ url('assets/node_modules/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ url('assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ url('assets/js/perfect-scrollbar.jquery.min.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ url('assets/js/waves.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ url('assets/js/sidebarmenu.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ url('assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <script src="{{ url('assets/node_modules/sparkline/jquery.sparkline.min.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{ url('assets/js/custom.min.js') }}"></script>
    <script src="{{ url('assets/js/backend/main.js') }}"></script>
    @yield('js')
</body>

</html>