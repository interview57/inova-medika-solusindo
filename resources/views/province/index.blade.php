@extends('layouts.main')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?= $title ?></h4>
    </div>
    <div class="col-md-7 align-self-center text-end">
        <div class="d-flex justify-content-end align-items-center">
            <div class="btn-group m-l-15">
                <a href="/province/create" type="button" class="btn btn-primary">Tambah Data</a>
            </div>
        </div>
    </div>
</div>
<div class="col-12">
    <div class="card">
        <div class="card-header bg-primary text-white">Daftar {{ $title }}</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-striped" id="list-table" data-action="{{ url('province-ajax') }}">
                    <thead>
                        <th width="30">No</th>
                        <th>Nama</th>
                        <th width="100">Aktif?</th>
                        <th width="30">Aksi</th>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection


@section('css')
<link href="{{ url('assets/node_modules/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
<link href="{{ url('assets/node_modules/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
<link href="{{ url('assets/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" >
@endsection

@section('js')
<script src="{{ url('assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
<script src="{{ url('assets/node_modules/toast-master/js/jquery.toast.js') }}"></script>
<script src="{{ url('assets/node_modules/datatables.net/js/jquery.dataTables.min.js') }}"></script>
@if(session()->has('notifSuccess'))
    <script>
    $(function(){
        $.toast({
        heading: 'Berhasil',
        text: '{{ session("notifSuccess") }}',
        position: 'top-right',
        loaderBg:'#ff6849',
        icon: 'success',
        hideAfter: 3500, 
        stack: 6
        });
    })
    </script>
@endif
<script>
$(function () {   
    var table = $('#list-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: $('#list-table').attr('data-action'),
    columns: [
        {data: 'DT_RowIndex', name: 'DT_RowIndex'},
        {data: 'name', name: 'name'},
        {data: 'is_active', name: 'is_active'},
        {
            data: 'action', 
            name: 'action', 
            orderable: true, 
            searchable: true
        },
    ]
    });

    $('body').on('click','.btn-delete',function(){
        var url = '/province/';
            url += $(this).attr('data-id')
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        Swal.fire({
          title             : "Apakah Anda Yakin ?",
          text              : "Data Yang Sudah Dihapus Tidak Bisa Dikembalikan!",
          icon              : "warning",
          showCancelButton  : true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor : "#d33",
          confirmButtonText : "Ya, Tetap Hapus!"
      }).then((result) => {
          if (result.value) {
              $.ajax({
                  url    : url,
                  type   : "delete",
                  success: function(data) {
                    table.ajax.reload();
                  }
              })
          }
      })
    })
});
</script>
@endsection