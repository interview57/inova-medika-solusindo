@extends('layouts.main')

@section('content')
<?php
    $name = $user->name;
    if(old('name')) $name = old('name');

    $email = $user->email;
    if(old('email')) $email = old('email');

    $is_active = '';
    if((strlen(old('is_active'))==0 && $user->is_active) || old('is_active')) $is_active = ' checked';
?>
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?= $title ?></h4>
    </div>
</div>
<div class="row">
    <form action="/{{ $dir }}/{{ $user->id }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="col-md-6">
            <div class="card">
                <div class="card-header p-0 bg-primary text-center">
                    <div class="mt-2 text-white card-title">Formulir</div>
                </div>
                <div class="card-body">
                    <div class="form-group @error('image') has-danger @enderror">
                        <label class="form-label required">Image User</label>
                        <input type="file" name="file" id="file" class="form-control">
                        @error('image')
                        <small class="form-control-feedback">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group @error('name') has-danger @enderror">
                        <label class="form-label required">Nama user</label>
                        <input value="{{ $name }}" type="text" name="name" id="name" class="form-control" placeholder="Nama User" autocomplete="off" required>
                        @error('name')
                        <small class="form-control-feedback">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group @error('email') has-danger @enderror">
                        <label class="form-label required">Email User</label>
                        <input value="{{ $email }}" type="email" name="email" id="email" class="form-control" placeholder="Email User" autocomplete="off" required>
                        @error('email')
                        <small class="form-control-feedback">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group @error('password') has-danger @enderror">
                        <label class="form-label">Password</label>
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password User" autocomplete="off">
                        @error('password')
                        <small class="form-control-feedback">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="form-label">Aktif?</label>
                        <div class="form-check form-switch">
                            <input type="checkbox" class="form-check-input input_switch"{{ $is_active }} id="is_active" value="1">
                          <input type="hidden" name="is_active" value="1">
                          <label class="form-check-label" for="is_active">Aktif</label>
                        </div> 
                    </div>
                </div>
                <div class="card-footer">
                    <div class="float-end">
                        <div class="btn-group m-l-15">
                            <a href="/{{ $dir }}" type="button" class="btn waves-effect waves-light btn-outline-primary">Kembali</a>
                            <button type="submit" class="btn btn-primary btn-save">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $('.input_switch').trigger('change');
</script>
@endsection