// Setup apps
var base_url = "";
var url = "";
var app_key = "";
var req_upload = false;
var lang = {};
var website = {};
$(function () {
	var dt_body = $("body").data();
	base_url = dt_body.base_url;
	url = dt_body.url;
	app_key = dt_body.app_key;
	getLang();
});
function getLang() {
	var lang_code = $('meta[http-equiv="content-language"]').attr("content");
	var req = base_url + "main/lang";
	fetch(req)
		.then((response) => response.json())
		.then((res) => {
			lang = res.lang;
			website = res.website;
			datatable_language = {
				search: lang.search + ":",
				info:
					lang.showing +
					" _START_ " +
					lang.to_tbl +
					" _END_ " +
					lang.of_tbl +
					" _TOTAL_ " +
					lang.entries,
				infoEmpty:
					lang.showing +
					" 0 " +
					lang.to_tbl +
					" 0 " +
					lang.of_tbl +
					" 0 " +
					lang.entries,
				lengthMenu: lang.show + " _MENU_ " + lang.entries,
				infoFiltered: lang.tbl_info_filtered,
				zeroRecords: lang.tbl_zero_records,
				paginate: {
					next: lang.next,
					previous: lang.previous,
				},
			};
			if (window["initSetup"]) {
				initSetup();
			}
		});
}
function langs(val) {
	if (lang[val]) {
		val = lang[val];
	} else if (val.length > 0) {
		var arr = val.split("_");
		val = "";
		$.each(arr, function (k, v) {
			if (k != 0) val += " ";
			val += upperCase(v);
		});
	}
	return val;
}
function upperCase(val) {
	val = val.toLowerCase().replace(/\b[a-z]/g, function (letter) {
		return letter.toUpperCase();
	});
	return val;
}

// setting ajax
$.ajaxSetup({
	beforeSend: function (xhr, settings) {
		if (!req_upload) {
			settings.data += "&app_key=" + app_key;
		} else {
			settings.data.append("app_key", app_key);
		}
	},
	complete: function (xhr, stat) {
		req_upload = false;
	},
	error: function (jqXHR, textStatus, errorThrown) {
		if (jqXHR.status) {
			if (jqXHR.status == "200") {
				mNotify("Invalid data response", "error", lang.information);
			} else if (jqXHR.status == "500") {
				mNotify("Internal server errors", "error", lang.information);
			} else if (jqXHR.status == "403") {
				mNotify("forbidden access", "error", lang.information);
			} else if (jqXHR.status == "404") {
				mNotify("Page not found", "error", lang.information);
			} else {
				mNotify("Errors " + jqXHR.status, "error", lang.information);
			}
		} else {
			console.log("Unknown errors status");
			// mNotify("Unknown errors status", "error", lang.information);
		}

		buttonLoading(true);
	},
});
function setValidation(e, data) {
	$.each(data, function (k, v) {
		var elmt = e.find('[name="' + k + '"]');
		if (elmt.length > 0) {
			var group = elmt.closest(".form-group");
			var val = group.find("label").html() + " " + v;
			group.addClass("has-danger");
			group.find(".form-control-feedback").html(val);
		}
	});
}
// end setting ajax

$(document).on(
	"click",
	"a.disabled,a[disabled],a[data-disabled]",
	function (e) {
		e.preventDefault();
	}
);

// End Setup apps
function mNotify(message, type, title, action) {
	var data = { type: type, text: message };
	if (title) data.title = title;
	if (action) {
		Swal.fire(data).then((result) => {
			window[action]();
		});
	} else {
		Swal.fire(data);
	}
}
var mConfirm =
	mConfirm ||
	(function ($) {
		"use strict";
		return {
			open: function (onConfirm, message, data) {
				if (!message) {
					message = lang.are_u_sure_delete + "?";
				}
				var title = lang.confirmation;
				var type = "question";
				Swal.fire({
					title: title,
					text: message,
					type: type,
					showCancelButton: true,
					cancelButtonText: lang.cancel,
					confirmButtonText: lang.continue,
					reverseButtons: true,
					confirmButtonColor: "#3085d6",
					cancelButtonColor: "#d33",
					allowOutsideClick: false,
					allowEscapeKey: false,
					showLoaderOnConfirm: true,
					preConfirm: function () {
						return new Promise(function (resolve) {
							setTimeout(function () {
								if (typeof onConfirm !== "undefined") {
									var act = window[onConfirm];
									if (data) {
										act(data);
									} else {
										act();
									}
								} else {
									mNotify("lanjutkan", "success", lang.information);
								}
							}, 1000);
						});
					},
				}).then((result) => {});
			},
			close: function () {
				Swal.close();
			},
		};
	})(jQuery);

// Modal Setup
var count_modal = 0;
function modalInfo(title, message, option) {
	count_modal += 1;
	var modal_size = "";
	var modal_button = "";
	if (option) {
		if (option.modal_size) modal_size = ` ` + option.modal_size;
		if (option.modal_button) modal_button = option.modal_button;
	}

	var html =
		`<div class="modal fade g-modal" id="g-modal-` +
		count_modal +
		`" tabindex="-1" data-bs-backdrop="static" aria-labelledby="g-modal-label-` +
		count_modal +
		`" aria-hidden="true">
	<div class="modal-dialog` +
		modal_size +
		` modal-dialog-centered modal-dialog-scrollable">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" id="g-modal-label-` +
		count_modal +
		`">` +
		title +
		`</h5>
		  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		</div>
		<div class="modal-body">` +
		message +
		`</div>
		<div class="modal-footer">
		  	<button type="button" class="btn btn-secondary btn-g-modal-close" data-bs-dismiss="modal">` +
		lang.close +
		`</button>` +
		modal_button +
		`</div>
	  </div>
	</div>
  </div>`;
	$(document).find("#general-modal").append(html);
	$(document)
		.find("#g-modal-" + count_modal)
		.modal("show");
}
$(document).on("hidden.bs.modal", function (e) {
	var elmt = $(e.target);
	if (elmt.hasClass("g-modal")) {
		elmt.remove();
	}
});
// End Modal Setup

var btn;
var msg_loading_before = "";
var msg_loading =
	'<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>Loading...';
var msg_loading_icon =
	'<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>';
function buttonLoading(close, e, arr) {
	if (!arr) arr = [];
	if (e) btn = e;
	if (btn) {
		if (!close) {
			msg_loading_before = btn.html();
			btn.prop("disabled", true);
			if (inArray("icon", arr)) {
				btn.html(msg_loading_icon);
			} else {
				btn.html(msg_loading);
			}
			$("body").find("button").prop("disabled", true);
			$("body").find("a").addClass("disabled");
		} else {
			btn.prop("disabled", false);
			btn.html(msg_loading_before);
			btn = null;
			$("body").find("button").prop("disabled", false);
			$("body").find("a").removeClass("disabled");
		}
	}
}

function inArray(text, arr) {
	var val = jQuery.inArray(text, arr);
	var status = true;
	if (val === -1) {
		status = false;
	}
	return status;
}
function inArrayKey(text, arr) {
	var val = jQuery.inArray(text, arr);
	return val;
}

function currencyToNumber(val) {
	var arr = val.split(website.CurrencySeparatorDecimal);
	var val = arr[0].split(website.CurrencySeparator);
	val = val.join("");
	if (arr.length > 1) {
		val += "." + arr[1];
	}
	if (val.length <= 0) val = "0";
	val = parseFloat(val);
	return val;
}
function numberToCurrency(val) {
	return numberFormat(
		val,
		website.CurrencyDecimal,
		website.CurrencySeparatorDecimal,
		website.CurrencySeparator
	);
}
function numberFormat(e, c, d, t, z) {
	var n = e,
		c = isNaN((c = Math.abs(c))) ? 2 : c,
		d = d == undefined ? "." : d,
		t = t == undefined ? "," : t,
		s = n < 0 ? "-" : "",
		i = String(parseInt((n = Math.abs(Number(n) || 0).toFixed(c)))),
		j = (j = i.length) > 3 ? j % 3 : 0;
	x = z == "negatif" ? false : true;
	var result =
		s +
		(j ? i.substr(0, j) + t : "") +
		i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) +
		(c
			? d +
			  Math.abs(n - i)
					.toFixed(c)
					.slice(2)
			: "");
	if (s == "-" && x == true) {
		result = "-" + result.replace("-", "") + "";
	}
	return result;
}

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
	var expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(";");
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == " ") {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function checkCookie(p1) {
	var user = getCookie(p1);
	if (user != "") {
		alert("Welcome again " + user);
	} else {
		user = prompt("Please enter your name:", "");
		if (user != "" && user != null) {
			setCookie("username", user, 365);
		}
	}
}

function goHome() {
	window.location = base_url;
}
