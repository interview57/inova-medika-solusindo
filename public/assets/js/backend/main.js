$("body").on("change", ".input_switch", function () {
	var arr = [];
	var group = $(this).closest(".form-switch");
	arr[0] = 'Tidak Aktif';
	arr[1] = 'Aktif';
	if ($(this).prop("checked")) {
		group.find("label").html(arr[1]);
		group.find('input[type="hidden"]').val(1);
	} else {
		group.find("label").html(arr[0]);
		group.find('input[type="hidden"]').val(0);
	}
});