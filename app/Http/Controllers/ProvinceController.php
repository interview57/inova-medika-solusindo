<?php

namespace App\Http\Controllers;

use App\Models\Province;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Mhelper;

class ProvinceController extends Controller
{

    public function index()
    {
        $data['title'] = 'Provinsi';
        return view('province.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = 'Tambah Provinsi';
        return view('province.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|min:3',
            'is_active' => 'required|in:1,0',
        ]);

        Province::create($data);

        return redirect('/province')->with([
            'notifSuccess' => 'Data telah ditambah',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function show(Province $province)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function edit(Province $province)
    {
        $data['title'] = 'Ubah Provinsi';
        $data['province'] = $province;
        return view('province.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Province $province)
    {
        $data = $request->validate([
            'name' => 'required|min:3',
            'is_active' => 'required|in:1,0',
        ]);

        $province->update($data);

        return redirect('/province')->with([
            'notifSuccess' => 'Data telah diubah',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function destroy(Province $province)
    {
        $province->delete();
    }

    public function ajax(Request $request){
        $data = Province::latest()->get();
        return DataTables::of($data)
            ->editColumn('is_active', function($row){
                return Mhelper::labelActive($row->is_active);
            })
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $actionBtn = '
                <div class="btn-group me-2 btn-group-sm btn-tbl" role="group" aria-label="Action Button">
                    <a href="/province/'.$row->id.'/edit" type="button" class="btn btn-secondary" title="ubah">
                        <i class="ti-pencil-alt"></i>
                    </a>
                    <a type="button" class="btn btn-danger btn-delete" data-id="'.$row->id.'" title="hapus">
                        <i class="ti-trash"></i>
                    </a>
                </div>';
                return $actionBtn;
            })
            ->rawColumns(['action','is_active'])
            ->make(true);
    }

    public function search(Request $request){
        $data = [];
        if($request->has('q')){
            $search = $request->q;
            $data =Province::select("id", "name")
            		->where('name', 'LIKE', "%$search%")
            		->where('is_active', '=', "1")
            		->get();
        }
        return response()->json($data);
    }
}
