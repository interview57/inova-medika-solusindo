<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Mhelper;

class EmployeeController extends Controller
{
    var $dir    = 'employee';
    var $title  = 'Pegawai';
    public function index()
    {
        $data['title'] = $this->title;
        $data['dir'] = $this->dir;
        return view($this->dir.'.index',$data);
    }

    
    public function create()
    {
        $data['title'] = 'Tambah '.$this->title;
        $data['dir'] = $this->dir;
        return view($this->dir.'.create',$data);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|min:3',
            'gender' => 'required|in:1,2',
            'is_active' => 'required|in:1,0',
        ]);
        $data['phone'] = $request->phone;
        $data['address'] = $request->address;

        Employee::create($data);

        return redirect('/'.$this->dir)->with([
            'notifSuccess' => 'Data telah ditambah',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    
    public function edit(Employee $employee)
    {
        $data['title'] = 'Ubah '.$this->title;
        $data['dir'] = $this->dir;
        $data['employee'] = $employee;
        return view($this->dir.'.edit',$data);
    }

    public function update(Request $request, Employee $employee)
    {
        $data = $request->validate([
            'name' => 'required|min:3',
            'gender' => 'required|in:1,2',
            'is_active' => 'required|in:1,0',
        ]);
        $data['phone'] = $request->phone;
        $data['address'] = $request->address;

        $employee->update($data);

        return redirect('/'.$this->dir)->with([
            'notifSuccess' => 'Data telah ditambah',
        ]);
    }

    public function destroy(Employee $employee)
    {
        $employee->delete();
    }

    public function list(Request $request){
        $data = Employee::latest()->get();
        return DataTables::of($data)
            ->editColumn('is_active', function($row){
                return Mhelper::labelActive($row->is_active);
            })
            ->editColumn('gender', function($row){
                return Mhelper::labelGender($row->gender);
            })
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $actionBtn = '
                <div class="btn-group me-2 btn-group-sm btn-tbl" role="group" aria-label="Action Button">
                    <a href="/'.$this->dir.'/'.$row->id.'/edit" type="button" class="btn btn-secondary" title="ubah">
                        <i class="ti-pencil-alt"></i>
                    </a>
                    <a type="button" class="btn btn-danger btn-delete" data-id="'.$row->id.'" title="hapus">
                        <i class="ti-trash"></i>
                    </a>
                </div>';
                return $actionBtn;
            })
            ->rawColumns(['action','is_active'])
            ->make(true);
    }
}
