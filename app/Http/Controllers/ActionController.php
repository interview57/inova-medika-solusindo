<?php

namespace App\Http\Controllers;

use App\Models\Action;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Mhelper;

class ActionController extends Controller
{
    var $dir    = 'action';
    var $title  = 'Tindakan';
    public function index()
    {
        $data['title'] = $this->title;
        $data['dir'] = $this->dir;
        return view($this->dir.'.index',$data);
    }

    public function create()
    {
        $data['title'] = 'Tambah '.$this->title;
        $data['dir'] = $this->dir;
        return view($this->dir.'.create',$data);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|min:1',
            'is_active' => 'required|in:1,0',
        ]);

        Action::create($data);

        return redirect('/'.$this->dir)->with([
            'notifSuccess' => 'Data telah ditambah',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function show(Action $action)
    {
        //
    }

    public function edit(Action $action)
    {
        $data['title'] = 'Ubah '.$this->title;
        $data['dir'] = $this->dir;
        $data['action'] = $action;
        return view($this->dir.'.edit',$data);
    }

    public function update(Request $request, Action $action)
    {
        $data = $request->validate([
            'name' => 'required|min:1',
            'is_active' => 'required|in:1,0',
        ]);

        $action->update($data);

        return redirect('/'.$this->dir)->with([
            'notifSuccess' => 'Data telah diubah',
        ]);
    }

    public function destroy(Action $action)
    {
        $action->delete();
    }

    public function list(Request $request){
        $data = Action::latest()->get();
        return DataTables::of($data)
            ->editColumn('is_active', function($row){
                return Mhelper::labelActive($row->is_active);
            })
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $actionBtn = '
                <div class="btn-group me-2 btn-group-sm btn-tbl" role="group" aria-label="Action Button">
                    <a href="/'.$this->dir.'/'.$row->id.'/edit" type="button" class="btn btn-secondary" title="ubah">
                        <i class="ti-pencil-alt"></i>
                    </a>
                    <a type="button" class="btn btn-danger btn-delete" data-id="'.$row->id.'" title="hapus">
                        <i class="ti-trash"></i>
                    </a>
                </div>';
                return $actionBtn;
            })
            ->rawColumns(['action','is_active'])
            ->make(true);
    }
}
