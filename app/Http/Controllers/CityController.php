<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Mhelper;

class CityController extends Controller
{
    var $dir    = 'city';
    var $title  = 'Kota/Kabupaten';
    public function index()
    {
        $data['title'] = $this->title;
        return view($this->dir.'.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = 'Tambah '.$this->title;
        return view($this->dir.'.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'province_id' => 'required',
            'name' => 'required|min:3',
            'is_active' => 'required|in:1,0',
        ]);

        City::create($data);

        return redirect('/city')->with([
            'notifSuccess' => 'Data telah ditambah',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        $data['title'] = 'Ubah '.$this->title;
        $data['dir'] = $this->dir;
        $data['city'] = $city;
        return view($this->dir.'.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        $data = $request->validate([
            'province_id' => 'required',
            'name' => 'required|min:3',
            'is_active' => 'required|in:1,0',
        ]);

        $city->update($data);

        return redirect('/city')->with([
            'notifSuccess' => 'Data telah diubah',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        $city->delete();
    }

    public function list(Request $request){
        $data = City::latest()->get();
        return DataTables::of($data)
            ->editColumn('is_active', function($row){
                return Mhelper::labelActive($row->is_active);
            })
            ->addIndexColumn()
            ->addColumn('province', function($row){
                return $row->province->name;
            })
            ->addColumn('action', function($row){
                $actionBtn = '
                <div class="btn-group me-2 btn-group-sm btn-tbl" role="group" aria-label="Action Button">
                    <a href="/city/'.$row->id.'/edit" type="button" class="btn btn-secondary" title="ubah">
                        <i class="ti-pencil-alt"></i>
                    </a>
                    <a type="button" class="btn btn-danger btn-delete" data-id="'.$row->id.'" title="hapus">
                        <i class="ti-trash"></i>
                    </a>
                </div>';
                return $actionBtn;
            })
            ->rawColumns(['action','is_active'])
            ->make(true);
    }

    public function search(Request $request){
        $data = [];
        if($request->has('search')){
            $search = $request->search;
            $data =City::select("id", "name")
            		->where('name', 'LIKE', "%$search%")
            		->where('is_active', '=', "1")
            		->where('province_id', '=', $request->province_id)
            		->get();
        }
        return response()->json($data);
    }
}
