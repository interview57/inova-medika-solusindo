<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;
use Mhelper;

class UserController extends Controller
{
    var $dir    = 'user';
    var $title  = 'User';
    public function index()
    {
        $data['title'] = $this->title;
        $data['dir'] = $this->dir;
        return view($this->dir.'.index',$data);
    }

    public function create()
    {
        $data['title'] = 'Tambah '.$this->title;
        $data['dir'] = $this->dir;
        return view($this->dir.'.create',$data);
    }

    public function store(Request $request)
    {
        $file = $request->file('file')->store('users');
        $data = $request->validate([
            'name' => 'required|min:3',
            'email' => 'required|email:dns',
            'password' => 'required|min:6',
            'is_active' => 'required|in:1,0',
        ]);
        $data['password'] = Hash::make($data['password']);
        if($file) $data['image'] = $file;

        User::create($data);

        return redirect('/'.$this->dir)->with([
            'notifSuccess' => 'Data telah ditambah',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    public function edit(User $user)
    {
        $data['title'] = 'Ubah '.$this->title;
        $data['dir'] = $this->dir;
        $data['user'] = $user;
        return view($this->dir.'.edit',$data);
    }

    public function update(Request $request, User $user)
    {
        $file = $request->file('file')->store('users');
        $validate = [
            'name' => 'required|min:3',
            'email' => 'required|email:dns',
            'is_active' => 'required|in:1,0',
        ];
        if($request->password) $validate['password'] = 'required|min:6';
        $data = $request->validate($validate);
        if($request->password) $data['password'] = Hash::make($data['password']);
        if($file) $data['image'] = $file;

        $user->update($data);

        return redirect('/'.$this->dir)->with([
            'notifSuccess' => 'Data telah diubah',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
    }

    public function active(Request $request){
        $user = User::where('id',$request->id)->first();
        $active = 1;
        if($user->is_active == 1) $active = 0;
        $user->is_active = $active;
        $user->save();
    }

    public function list(Request $request){
        $data = User::latest()->get();
        return DataTables::of($data)
            ->editColumn('is_active', function($row){
                return Mhelper::labelActive($row->is_active);
            })
            ->editColumn('image', function($row){
                $img = url('storage/app/'.$row->image);
                $img = Storage::url('public/'.$row->image);
                return '<img src="'.$img.'" width="50"/>';
            })
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = mHelper::btnActive($row);
                $actionBtn = '
                <div class="btn-group me-2 btn-group-sm btn-tbl" role="group" aria-label="Action Button">
                    <a href="/'.$this->dir.'/'.$row->id.'/edit" type="button" class="btn btn-secondary" title="ubah">
                        <i class="ti-pencil-alt"></i>
                    </a>
                    '.$btn.'
                    <a type="button" class="btn btn-danger btn-delete" data-id="'.$row->id.'" title="hapus">
                        <i class="ti-trash"></i>
                    </a>
                </div>';
                return $actionBtn;
            })
            ->rawColumns(['action','is_active','image'])
            ->make(true);
    }
}
