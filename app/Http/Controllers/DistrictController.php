<?php

namespace App\Http\Controllers;

use App\Models\District;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Mhelper;

class DistrictController extends Controller
{
    var $dir    = 'district';
    var $title  = 'Kecamatan';
    public function index()
    {
        $data['title'] = $this->title;
        $data['dir'] = $this->dir;
        return view($this->dir.'.index',$data);
    }

    
    public function create()
    {
        $data['title'] = 'Tambah '.$this->title;
        $data['dir'] = $this->dir;
        return view($this->dir.'.create',$data);
    }

    
    public function store(Request $request)
    {
        $data = $request->validate([
            'province_id' => 'required',
            'city_id' => 'required',
            'name' => 'required|min:3',
            'is_active' => 'required|in:1,0',
        ]);

        District::create($data);

        return redirect('/'.$this->dir)->with([
            'notifSuccess' => 'Data telah ditambah',
        ]);
    }

    
    public function show(District $district)
    {
        //
    }

    
    public function edit(District $district)
    {
        $data['title'] = 'Ubah '.$this->title;
        $data['dir'] = $this->dir;
        $data['district'] = $district;
        return view($this->dir.'.edit',$data);
    }

    
    public function update(Request $request, District $district)
    {
        $data = $request->validate([
            'province_id' => 'required',
            'city_id' => 'required',
            'name' => 'required|min:3',
            'is_active' => 'required|in:1,0',
        ]);

        $district->update($data);

        return redirect('/'.$this->dir)->with([
            'notifSuccess' => 'Data telah diubah',
        ]);
    }

    
    public function destroy(District $district)
    {
        $district->delete();
    }

    public function list(Request $request){
        $data = District::latest()->get();
        return DataTables::of($data)
            ->editColumn('is_active', function($row){
                return Mhelper::labelActive($row->is_active);
            })
            ->addIndexColumn()
            ->addColumn('province', function($row){
                return $row->province->name;
            })
            ->addColumn('city', function($row){
                return $row->city->name;
            })
            ->addColumn('action', function($row){
                $actionBtn = '
                <div class="btn-group me-2 btn-group-sm btn-tbl" role="group" aria-label="Action Button">
                    <a href="/'.$this->dir.'/'.$row->id.'/edit" type="button" class="btn btn-secondary" title="ubah">
                        <i class="ti-pencil-alt"></i>
                    </a>
                    <a type="button" class="btn btn-danger btn-delete" data-id="'.$row->id.'" title="hapus">
                        <i class="ti-trash"></i>
                    </a>
                </div>';
                return $actionBtn;
            })
            ->rawColumns(['action','is_active'])
            ->make(true);
    }
}
