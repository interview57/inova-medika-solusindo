<?php
namespace App\Helpers;
 
class Main {
    public static function labelActive($val,$style=true){
        $label = '<span class="label label-danger">Tidak Aktif</span>';
        if($val) $label = '<span class="label label-success">Aktif</span>';
        if(!$style) $label = strip_tags($label);
        return $label;
    }

    public static function labelGender($val){
        $label = "Laki-laki";
        if($val == 2) $label = "Perempuan";
        return $label;
    }

    public static function btnActive($row){
        $active = 1;
        if($row->is_active) $active = 0;
        $label = Main::labelActive($active,false);
        $btn = '<a type="button" class="btn btn-info btn-nonactive" data-id="'.$row->id.'" title="'.$label.'">
            <i class="ti-check"></i>
        </a>';
        return $btn;
    }
}