<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\ProvinceController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\DistrictController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\ActionController;
use App\Http\Controllers\MedicineController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/','/login');

// login
Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate']);

// middleware
Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', [DashboardController::class, 'index']);
    Route::post('/logout', [DashboardController::class, 'logout']);

    Route::resource('/province', ProvinceController::class);
    Route::get('/province-ajax',[ProvinceController::class,'ajax']);
    Route::get('/province-autocomplete-search', [ProvinceController::class,'search']);

    Route::resource('/city', CityController::class);
    Route::get('/city-list',[CityController::class,'list']);
    Route::get('/city-autocomplete-search', [CityController::class,'search']);

    Route::resource('/district', DistrictController::class);
    Route::get('/district-list',[DistrictController::class,'list']);
    Route::get('/district-autocomplete-search', [DistrictController::class,'search']);

    Route::resource('/user', UserController::class);
    Route::get('/user-list',[UserController::class,'list']);
    Route::get('/user-active',[UserController::class,'active']);

    Route::resource('/employee', EmployeeController::class);
    Route::get('/employee-list',[EmployeeController::class,'list']);

    Route::resource('/action', ActionController::class);
    Route::get('/action-list',[ActionController::class,'list']);

    Route::resource('/medicine', MedicineController::class);
    Route::get('/medicine-list',[MedicineController::class,'list']);
});

Route::get('/roles', [PermissionController::class,'Permission']);